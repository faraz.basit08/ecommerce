<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\categorie;


class CategoriesController extends Controller
{
    public function allcategories()
    {
       $data = categorie::get();
        return view('ProductCategories/allcategories',['categories'=>$data]);
    }
    public function createcategory()
    {
        return view('ProductCategories/createcategory');
    }

    // public function postcategory()
    // {
    //     // return categorie::create([
    //     //     'name' => $data['name'],
    //     //     ]);
    // }
    
}
