<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;



Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/sidebar', [App\Http\Controllers\HomeController::class, 'sidebar'])->name('sidebar');
Route::get('/allcategories', [App\Http\Controllers\CategoriesController::class, 'allcategories'])->name('allcategories');
// Route::get('/createcategory', [App\Http\Controllers\CategoriesController::class, 'postcategory'])->name('createcategory');
Route::get('/venders', [App\Http\Controllers\HomeController::class, 'venders'])->name('venders');
Route::get('/vendersupdate/{id}', [App\Http\Controllers\HomeController::class, 'vendersupdate'])->name('vendersupdate');
Route::post('vendersupdate', [App\Http\Controllers\HomeController::class, 'vendersupdatedata'])->name('vendersupdatedata');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
// Route::get('/venderbarfetch', [App\Http\Controllers\HomeController::class, 'venderbarfetch'])->name('venderbarfetch');

// Permisions Fetch And Update Start ////////////////
Route::get('/venderbarfetch/{id}', [App\Http\Controllers\HomeController::class, 'venderbarfetch'])->name('venderbarfetch');
Route::post('venderbarfetch', [App\Http\Controllers\HomeController::class, 'venderspermisionbar'])->name('venderspermisionbar');
// Permisions Fetch And Update Start ////////////////
